function getCORSBody (data, httpMethod) {
  const request = {
    method: httpMethod || 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  }
  if (data) {
    request.body = JSON.stringify(data)
  }
  return request
}

async function generalRequest (target, options = { body: undefined, httpMethod: undefined }) {
  if (options.httpMethod && !['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH'].includes(options.httpMethod)) {
    throw Error(`Invalid httpMethod '${options.httpMethod}'`)
  }
  const errorKeys = ['status', 'title', 'detail']
  const fetchBody = getCORSBody(options.body, options.httpMethod || (options.body ? 'POST' : 'GET'))
  const request = fetch(target, fetchBody)

  return request.then(
    response => {
      const defaultErrorResponse = {
        status: response.status,
        title: response.statusText,
        detail: 'An error occurred while accessing ' + response.url,
        instance: response.url
      }
      return {
        defaultErrorResponse: defaultErrorResponse,
        responseData: response.headers.get('Content-Length') !== '0' ? response.json() : { status: response.status }
      }
    }
  ).then(
    async data => {
      // console.log(data)
      const responseData = await data.responseData
      if (
        Math.floor(data.defaultErrorResponse.status / 100) !== 2 && (
          responseData.status === undefined ||
          Math.floor(responseData.status / 100) !== 2)
      ) {
        throw (errorKeys.every(key => Object.keys(responseData).includes(key)))
          ? responseData
          : data.defaultErrorResponse
      } else {
        return responseData
      }
    }
  ).catch(
    err => {
      const defaultError = {
        status: 500,
        title: err.name,
        detail: 'A javascript error occurred while accessing ' + target + ' - ' + err.message,
        instance: target
      }
      throw (errorKeys.every(key => Object.keys(err).includes(key)))
        ? err
        : defaultError
    }
  )
}

export default generalRequest
