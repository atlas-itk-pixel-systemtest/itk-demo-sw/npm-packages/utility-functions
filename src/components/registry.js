import generalRequest from './generalRequest'

const convertToKvs = (data) =>
  Object.entries(data).map(([key, value]) => { return { key, value } })

const init = (host = 'http://localhost:2379') => {
  const client = host

  const getAll = async (prefix = '') => {
    const pre = prefix
    const key = pre.endsWith('/') ? pre.slice(0, pre.length - 1) : pre
    try {
      const data = await generalRequest(`${client}/get_prefix?prefix=${key}`)
      const convert = convertToKvs(data)
      return convert
    } catch (e) {
      if (e.status === 404) {
        console.warn('KeyError: ', key, ' could not be found.')
        return []
      }
      throw e
    }
  }
  return {
    getAll,
    getAllKeysOf: async (prefix = '') => {
      const pre = prefix
      const key = pre.endsWith('/') ? pre.slice(0, pre.length - 1) : pre
      const kvs = await getAll(key)
      return kvs.map(kv => {
        if (!kv.key.includes('/')) return null
        const newkey = kv.key.split(key + '/')[1]
        if (newkey === undefined) return null
        return newkey.split('/')[0]
      }).reduce((collection, key) => {
        if (key !== null && !collection.includes(key)) collection.push(key)
        return collection
      }, [])
    },
    get: async (key = '') => {
      try {
        const data = await generalRequest(`${client}/get?key=${key}`)
        return convertToKvs(data)[0]
      } catch (e) {
        if (e.status === 404) {
          console.warn('KeyError: ', key, ' could not be found.')
          return { key: null, value: 'Fehler' }
        }
        throw e
      }
    },
    set: async (key, value) => {
      return await generalRequest(`${client}/set`, { key, value })
    },
    /**
     *
     * @param {[key, value][]} keyValuePairs
     * @returns
     */
    setMany: async (keyValuePairs = [['key', 'value']]) => {
      return await generalRequest(`${client}/set_many`, {
        kv_dict: keyValuePairs.reduce((obj, [key, value]) => {
          obj[key] = value
          return obj
        }, {})
      })
    },
    /**
     *
     * @param {string[]} keys
     * @returns
     */
    watch: (keys = []) => {
      return new EventSource(`${client}/watch?keys=${keys.join(';')}`)
    }
  }
}

export default init
