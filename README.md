# utility-functions

Utility js functions for usage in itk-demo microservices.

## How to change utility-functions and publish to private registry

A quick quide to how individual utility-functions can be adapted and others can be added. 
**Make sure that any changes to existing utility-functions are backwards compatible!**

### Checking out the git repository
The following explanation assumes that you cloned the repository into
```
mylocal-itk-demo-sw/npm-packages/utility-functions/
```
And test the changes with the local project living in
```
mylocal-itk-demo-sw/itk-demo-template/
```
### Building
After you have coded your changes you can build them using:
```
cd mylocal-itk-demo-sw/npm-packages/utility-functions/
npm i
npm run build
```
The built React utility-functions are then stored in the `build/`directory in the project's root folder. 
Before you push your changes to gitlab or publish them to the package registry, you can import them into a local project. This allows you to test your changes and avoid messing up the public registry. Install the locally built utility-functions from your project as follows: 
```
cd mylocal-itk-demo-sw/itk-demo-template/ui
npm i ../../npm-packages/utility-functions/
```
*Note: Package does not use react, therefor linking is not needed.*
You can access the local utility-functions via `import { ... } from '@itk_demo_sw/utility-functions'`. To check that you are actually using your local changes, look at the local `node_modules` folder in your testing project. If the `@itk_demo_sw/utility-functions` entry is a symbolic link to your local installation of this repository, everything worked fine.
Once you are sure that your changes work as expected you have to commit your changes to git. Then you can continue publishing them to the package registry. 

### Publishing
**Note: This still has to be done manually!** Eventually gitlab CI should automatically integrate changes to utility-functions. For now this has not yet been implemented. 

To publish first change the version of your package. You have three choices of version incrementing: major, minor and patch. Use 'minor' whenever you add a new hook or make larger changes to an existing one. Use 'patch' whenever you make small changes to an existing hook or fix a bug. Never use 'major'.
The incrementing can by done via:
```
npm version [minor | patch]
```
Once you have done this, publish your changes to the package registry using:
```
npm publish
```
Now all your changes should be available in the registry.

To authenticate via a token configure npm 
```
npm config set  //gitlab.cern.ch/api/v4/projects/123904/packages/npm/:_authToken=[GITLAB_TOKEN]
```
